using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebDiary.Data;
using WebDiary.Models;

namespace WebDiary.Pages.Entries
{
    public class DetailsModel : PageModel
    {
        private readonly WebDiary.Data.EntryContext _context;

        public DetailsModel(WebDiary.Data.EntryContext context)
        {
            _context = context;
        }

      public Entry Entry { get; set; } = default!; 

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Entry == null)
            {
                return NotFound();
            }

            var entry = await _context.Entry.FirstOrDefaultAsync(m => m.Id == id);
            if (entry == null)
            {
                return NotFound();
            }
            else 
            {
                Entry = entry;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null || _context.Entry == null)
            {
                return NotFound();
            }

            var entry = await _context.Entry.FirstOrDefaultAsync(m => m.Id == id);
            if (entry == null)
            {
                return NotFound();
            }


            byte[] array = UTF8Encoding.UTF8.GetBytes(entry.Title + "\n" + entry.CreationDate.ToString("MM/dd/yyyy HH:mm:ss") + "\n\n" + entry.Text);

            return File(array, "text/plain", entry.Title + ".txt");
        }
    }
}
