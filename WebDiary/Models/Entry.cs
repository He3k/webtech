using System.ComponentModel.DataAnnotations;

namespace WebDiary.Models;

public class Entry
{
    public int Id { get; set; }

    public string? Title { get; set; }
    public string? Text { get; set; }

    [DataType(DataType.Date)]
    public DateTime CreationDate { get; set; }
}
