using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebDiary.Models;

namespace WebDiary.Data
{
    public class EntryContext : DbContext
    {
        public EntryContext (DbContextOptions<EntryContext> options)
            : base(options)
        {
        }

        public DbSet<WebDiary.Models.Entry> Entry { get; set; } = default!;
    }
}
