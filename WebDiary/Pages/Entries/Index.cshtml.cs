using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebDiary.Data;
using WebDiary.Models;

namespace WebDiary.Pages.Entries
{
    public class IndexModel : PageModel
    {
        private readonly WebDiary.Data.EntryContext _context;

        public IndexModel(WebDiary.Data.EntryContext context)
        {
            _context = context;
        }

        public IList<Entry> Entry { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Entry != null)
            {
                Entry = await _context.Entry.ToListAsync();
            }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            string diary = "WebDiary\n";


            foreach (var entry in _context.Entry)
            {
                if (entry != null)
                {
                    diary += "\n\n" + entry.Title + "\n" + entry.CreationDate.ToString("MM/dd/yyyy HH:mm:ss") + "\n\n" + entry.Text;
                }
            }

            byte[] array = UTF8Encoding.UTF8.GetBytes(diary);

            return File(array, "text/plain", "WebDiary" + DateTime.Now.ToString("MM-dd-yyyy") + ".txt");
        }
    }
}
